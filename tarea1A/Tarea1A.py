#Tarea n° 1A Tomás Cortez


import glfw
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np
import transformations as tr
import scene_graph_1b as sg
import sys

# We will use 32 bits data, so an integer has 4 bytes
# 1 byte = 8 bits
INT_BYTES = 4

# A class to store the application control
class Controller:
    fillPolygon = True
    listaFish = 0
    createFish = False
    leftClickOn = False
    mousePos = (0.0, 0.0)
    

# we will use the global controller as communication with the callback function
controller = Controller()


def on_key(window, key, scancode, action, mods):

    if action != glfw.PRESS:
        return
    
    global controller

    if key == glfw.KEY_ESCAPE:
        print("Bye bye")
        sys.exit()
    
    if key == glfw.KEY_ENTER:
        controller.createFish = not controller.createFish
        
   

    else:
        print('Unknown key')
        
      
def cursor_pos_callback(window, x, y):
    global controller
    controller.mousePos = (x,y)


    
def mouse_button_callback(window, button, action, mods):

    global controller

    """
    glfw.MOUSE_BUTTON_1: left click
    glfw.MOUSE_BUTTON_2: right click
    glfw.MOUSE_BUTTON_3: scroll click
    """

    if (action == glfw.PRESS or action == glfw.REPEAT):
        if (button == glfw.MOUSE_BUTTON_1):
            controller.leftClickOn = True
            

        if (button == glfw.MOUSE_BUTTON_2):
            print("Mouse click - button 2:")

        if (button == glfw.MOUSE_BUTTON_3):
            print("Mouse click - button 3")

    elif (action ==glfw.RELEASE):
        if (button == glfw.MOUSE_BUTTON_1):
            controller.leftClickOn = False       

    
    
def createPolygon(nlados, radio, r0, g0, b0, r1, g1, b1):
    #Crea Cualquier polígono, implementar degradados no es tan fácil
    
    gpuShape = sg.GPUShape()
    
    listaVertices =  [0.0, 0.0, 0.0, r0, g0, b0]
    i=1
    while i <= (nlados):
        listaVertices += [radio*np.cos(2*np.pi*i/nlados),
                          radio*np.sin(2*np.pi*i/nlados),
                          0.0, r1, g1, b1]
        i += 1
	
    vertexData = np.array(listaVertices, dtype= np.float32)

    UnionVertices = []
    h=1
    while h <=(nlados):
        if h < nlados:
            UnionVertices += [0, h, h+1]
        else:
            UnionVertices += [0, h, 1]
        h += 1    
			
    indices = np.array(UnionVertices, dtype= np.uint32)
    gpuShape.size = len(indices)
    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape
    

def createFishTank():
    
    #Agua
    water = sg.SceneGraphNode("water")
    water.transform = tr.uniformScale(2)
    water.childs += [createPolygon(4, 1, 
    0.0, 0.0, 0.0,
    0.0, 0.3 ,0.8)]
    
    waterRotated = sg.SceneGraphNode("waterRotated")
    waterRotated.transform = tr.rotationZ(np.pi*0.25)
    waterRotated.childs += [water]

    #Arena
    sand1 = sg.SceneGraphNode("sand")
    sand1.transform = tr.scale(2.5,0.5,0)
    sand1.childs += [createPolygon(7, 0.5,
    0.86, 0.81, 0.71,
    0.76, 0.70, 0.50)]
    
    sand1Translated = sg.SceneGraphNode("sand1Translated")
    sand1Translated.transform = tr.translate(0,-0.88,0)
    sand1Translated.childs += [sand1]
    
    sand2 = sg.SceneGraphNode("sand2")
    sand2.transform = tr.scale(5, 1.0, 0)
    sand2.childs += [createPolygon(3,0.3,
    0.76, 0.70, 0.50,
    0.51, 0.45, 0.25)]
    
    sand2Translated = sg.SceneGraphNode("sand2Translated")
    sand2Translated.transform = tr.translate(-0.4, -1, 0.0)
    sand2Translated.childs += [sand2]
    
    sand3 = sg.SceneGraphNode("sand3")
    sand3.transform = tr.scale(9, 1.0, 0)
    sand3.childs += [createPolygon(9,0.3,
    0.69, 0.62, 0.37,
    0.51, 0.45, 0.25)]
    
    sand3Translated = sg.SceneGraphNode("sand3Translated")
    sand3Translated.transform = tr.translate(-0.4, -1, 0.0)
    sand3Translated.childs += [sand3]
    
    #Roca
    rock = sg.SceneGraphNode("rock")
    rock.transform = tr.matmul([tr.shearing(0.2, 0.0, 0.0, 0.0, 0.0, 0.0),
    tr.scale(2, 1.0, 0.0)])
    rock.childs += [createPolygon(6, 0.15,
    0.0, 0.0, 0.0,
    0.3, 0.3, 0.3)]
    
    rockTranslated1 = sg.SceneGraphNode("rockTranslated1")
    rockTranslated1.transform = tr.translate(0.12, -0.75, 0.0)
    rockTranslated1. childs += [rock]
    
    #Algas
    seaweedbase = sg.SceneGraphNode("seaweedbase")
    seaweedbase.transform = tr.shearing(2, 0.0, 0.0, 0.0, 0.0, 0.0)
    seaweedbase.childs += [createPolygon(4, 0.1,
    0.0, 1.0, 0.0,
    0.0, 0.2, 0.0)]

    seaweedmirror = sg.SceneGraphNode("seaweedmirror")
    seaweedmirror.transform = tr.shearing(-2, 0.0, 0.0, 0.0, 0.0, 0.0)
    seaweedmirror.childs += [createPolygon(4, 0.1,
    0.0, 1.0, 0.0,
    0.0, 0.2, 0.0)]

    seaweedLower = sg.SceneGraphNode("seaweedLower")
    seaweedLower.transform = tr.translate(0.1, -0.12, 0.0)
    seaweedLower.childs += [seaweedbase]
    
    seaweedMiddle = sg.SceneGraphNode("seaweedMiddle")
    seaweedMiddle.transform = tr.rotationZ(np.pi*0.25)
    seaweedMiddle.childs += [seaweedbase]
    
    seaweedUpper = sg.SceneGraphNode("seaweedUpper")
    seaweedUpper.transform = tr.translate(-0.22, -0.12, 0.0)
    seaweedUpper.childs += [seaweedmirror]
    
    seaweedTranslated = sg.SceneGraphNode("seaweedTranslated")
    seaweedTranslated.transform = tr.translate(0.68, -0.75, 0)
    seaweedTranslated.childs += [seaweedLower]
    seaweedTranslated.childs += [seaweedMiddle]
    seaweedTranslated.childs += [seaweedUpper]
    
    seaweedTranslated2 = sg.SceneGraphNode("seaweedTranslated2")
    seaweedTranslated2.transform = tr.translate(-0.6, -0.8, 0.0)
    seaweedTranslated2.childs += [seaweedLower]
    seaweedTranslated2.childs += [seaweedMiddle]
    seaweedTranslated2.childs += [seaweedUpper]

    #Fondo
    seaFloor = sg.SceneGraphNode("seaFloor")
    seaFloor.childs += [sand1Translated]
    seaFloor.childs += [rockTranslated1]
    seaFloor.childs += [sand3Translated]
    seaFloor.childs += [sand2Translated]
    seaFloor.childs += [seaweedTranslated]
    seaFloor.childs += [seaweedTranslated2]
     
    #lista de peces
    
    pecesLista = sg.SceneGraphNode("pecesLista")
    pecesLista.transform = tr.identity()
    pecesLista.childs += []
    
    #World
    fishTank = sg.SceneGraphNode("fishTank")
    fishTank.transform = tr.identity()
    fishTank.childs += [waterRotated]
    fishTank.childs += [seaFloor]
    fishTank.childs += [pecesLista]
    
    return fishTank
    
def createFishBody(n):
    #Aquí comienzan los peces 
    
    #Partes comunes 
    
    eye = sg.SceneGraphNode("eye")
    eye.transform = tr.identity()
    eye.childs += [createPolygon(30, 0.025,
    1.0, 1.0, 1.0,
    1.0, 1.0, 1.0)]
    
    pupil = sg.SceneGraphNode("pupil")
    pupil.transform = tr.identity()
    pupil.childs += [createPolygon(30, 0.008,
    0.0, 0.0, 0.0,
    0.0, 0.0, 0.0)]
    
    fishEye = sg.SceneGraphNode("fishEye")
    fishEye.transform = tr.translate(-0.11, 0.03, 0)
    fishEye.childs += [eye]
    fishEye.childs += [pupil]
    
    tail = sg.SceneGraphNode("tail")
    tail.transform = tr.rotationZ(np.pi)
    tail.childs += [createPolygon(3, 0.1,
    1.0, 1.0, 1.0,
    1.0, 1.0, 0.0)]
    
    fishTail = sg.SceneGraphNode("fishTail")
    fishTail.transform = tr.translate(0.27, 0.0, 0.0)
    fishTail.childs += [tail]
    
    upperFin = sg.SceneGraphNode("upperFin")
    upperFin.transform = tr.matmul([tr.rotationZ(np.pi*0.5 + 0.25),
    tr.translate(0.0, 0.08, 0.0)])
    upperFin.childs += [tail]
    
    lowerFin = sg.SceneGraphNode("lowerFin")
    lowerFin.transform = tr.matmul([tr.rotationZ(-np.pi*0.5 - 0.25),
    tr.translate(0.0, -0.065, 0.0)])
    lowerFin.childs += [tail]
    
    #Partes propias
    
    fish1body = sg.SceneGraphNode("fish1body")
    fish1body.transform = tr.scale(1.2 ,0.5, 0)
    fish1body.childs += [createPolygon(11, 0.2,
    1.0, 0.2, 0.5,
    0.5, 0.0, 0.9)]
    
    fish2body = sg.SceneGraphNode("fish2body")
    fish2body.transform = tr.scale(1.2 ,0.5, 0)
    fish2body.childs += [createPolygon(11, 0.2,
    1.0, 0.0, 0.0,
    1.0, 0.55, 0.41)]
    
    fish3body = sg.SceneGraphNode("fish3body")
    fish3body.transform = tr.scale(1.2 ,0.5, 0)
    fish3body.childs += [createPolygon(11, 0.2,
    1.0, 0.55, 0.41,
    1.0, 1.0, 0.0)]
    
    #Peces base
    
    fish1Base = sg.SceneGraphNode("fish1Base") 
    fish1Base.transform = tr.identity()
    fish1Base.childs += [fishTail]
    fish1Base.childs += [fish1body]
    fish1Base.childs += [fishEye]
    
    fish2Base = sg.SceneGraphNode("fish2Base")
    fish2Base.transform = tr.identity()
    fish2Base.childs += [fishTail]
    fish2Base.childs += [upperFin]
    fish2Base.childs += [lowerFin]
    fish2Base.childs += [fish2body]
    fish2Base.childs += [fishEye] 
  
    
    fish3Base = sg.SceneGraphNode("fish3Base")
    fish3Base.transform = tr.identity()
    fish3Base.childs += [fishTail]
    fish3Base.childs += [upperFin]
    fish3Base.childs += [fish3body]
    fish3Base.childs += [fishEye]
    
    if n == 1:
        return fish1Base 
        
    elif n == 2:
        return fish2Base
    
    else:
        return fish3Base


def createFish(fishTank):
   
    controller.listaFish += 1
    
    baseName = "fish_"
    
    newNode = sg.SceneGraphNode(baseName + str(controller.listaFish))
    
    newNode.posx = np.random.random() - 0.5
    newNode.posy = np.random.random() - 0.5
    newNode.xPattern = np.random.randint(4)
    newNode.yPattern = np.random.randint(4)
    newNode.transform = tr.translate(newNode.posx, newNode.posy, 0)
    
    fishBodyPattern = np.random.randint(1,5)
    fishBody = createFishBody(fishBodyPattern)
    newNode.childs += [fishBody]
    
  
    
    
    list = sg.findNode(fishTank, "pecesLista")
    list.childs += [newNode]
    
    return fishTank 
    
def animatedMovement(tipoX, tipoY, posx, posy, theta):
    horizontal = None
    vertical = None
    
    if tipoX == 0:
        horizontal = posx
        
    elif tipoX == 1:
        horizontal = theta + posx
        
    elif tipoX == 2:
        horizontal = np.cos(theta) + posx
    
    elif tipoX == 3:
        horizontal = (np.sin(theta))**2 + posx
    
    
    if tipoY == 0:
        vertical = posy
        
    elif tipoY == 1:
        vertical = theta + posy
    
    elif tipoY == 2:
        vertical = np.cos(theta) + posy
    
    elif tipoY == 3:
        vertical = (np.sin(theta))**2 + posy
    
    while  horizontal > 0.68 or horizontal < -0.68:
        valor = int(horizontal/0.68) 
        
        if (abs((valor-1)/4) - abs(int((valor-1)/4))) == 0:
            horizontal = (abs(valor) + 1)*0.68 - horizontal
            
        elif (abs((((valor/2)-1)/2)) - abs(int((((valor/2)-1)/2)))) == 0:
            horizontal = abs(valor)*0.68 - horizontal
        
        elif (abs((((valor-1)/2)-1)/2)-abs(int((((valor-1)/2)-1)/2))) == 0:
            horizontal = horizontal - 0.68 * (abs(valor) + 1)
            
        elif (abs(valor/4) - abs(int(valor/4))) == 0:
            horizontal = horizontal - 0.68 * abs(valor)
    
    while vertical > 0.85 or vertical < -0.85:
        valor = int(vertical/0.85)
        
        if (abs((valor-1)/4) - abs(int((valor-1)/4))) == 0:
            vertical = (abs(valor) + 1)*0.85 - vertical
            
        elif (abs((((valor/2)-1)/2)) - abs(int((((valor/2)-1)/2)))) == 0:
            vertical = abs(valor)*0.85 - vertical
        
        elif (abs((((valor-1)/2)-1)/2)-abs(int((((valor-1)/2)-1)/2))) == 0:
            vertical = vertical - 0.85 * (abs(valor) + 1)
            
        elif (abs(valor/4) - abs(int(valor/4))) == 0:
            vertical = vertical - 0.85 * abs(valor)
        
    
    finalMatrix = tr.translate(horizontal, vertical, 0)
    return finalMatrix
    
if __name__ == "__main__":

    print("Bienvenido")
    # Initialize glfw
    if not glfw.init():
        sys.exit()

    width = 600
    height = 600

    window = glfw.create_window(width, height, "Lake Valor Fishtank", None, None)

    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)

    # Connecting the callback function 'on_key' to handle keyboard events
    glfw.set_key_callback(window, on_key)
    
     # Connecting callback functions to handle mouse events:
    # - Cursor moving over the window
    # - Mouse buttons input
    glfw.set_cursor_pos_callback(window, cursor_pos_callback)
    glfw.set_mouse_button_callback(window, mouse_button_callback)

    # Assembling the shader program (pipeline) with both shaders
    shaderProgram = sg.basicShaderProgram()
    
    # Telling OpenGL to use our shader program
    glUseProgram(shaderProgram)

    # Setting up the clear screen color
    glClearColor(0.85, 0.85, 0.85, 1.0)

    # Creating shapes on GPU memory
    fishTank = createFishTank()
    
    fishTank = createFish(fishTank)
    
    fishTank = createFish(fishTank)
    
    fishTank = createFish(fishTank)
    
    fishTank = createFish(fishTank)
    
    fishTank = createFish(fishTank)
    
    
    
    # Our shapes here are always fully painted
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
    
    contador = 0
    #frecuencia del Shearing
    separador = 100
    #Margen de error del mouse para clickear un pez
    delta = 0.1
    

    while not glfw.window_should_close(window):
        # Using GLFW to check for input events
        glfw.poll_events()

        # Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT)
        
        theta = glfw.get_time()*0.5
        
        if controller.createFish == True:
            fishTank = createFish(fishTank)
            controller.createFish = False 
        
        
        FishList = sg.findNode(fishTank,"pecesLista").childs
        for fish in FishList:
            movimiento = animatedMovement(fish.xPattern, fish.yPattern,
            fish.posx, fish.posy, theta)
            
            fish.transform = movimiento
        
        if controller.leftClickOn:
            mousePosX = 2 * (controller.mousePos[0] - width/2) / width
            mousePosY = 2 * (height/2 - controller.mousePos[1]) / height
           
            for fish in FishList:
                position = sg.findPosition(fishTank, fish.name, tr.identity())
                
                if mousePosX -delta<=position[0] <= mousePosX+delta and mousePosY -delta<=position[1]<=mousePosY+delta:
                    FishList.remove(fish)
                
                

        
        if contador <= separador:
            sg.drawSceneGraphNode(fishTank, shaderProgram, tr.identity())
            contador += 1 
            
            
        elif separador < contador <= separador * 2 :
            sg.drawSceneGraphNode(fishTank, shaderProgram, tr.shearing(0.1, 0.0, 0.0, 0.0, 0.0, 0.0))
            contador += 1
        
        elif separador * 2 < contador <= separador * 3 :
            sg.drawSceneGraphNode(fishTank, shaderProgram, tr.shearing(-0.1, 0.0, 0.0, 0.0, 0.0, 0.0))
            contador += 1
            
        else:
            sg.drawSceneGraphNode(fishTank, shaderProgram, tr.identity())
            contador = 0

        # Once the render is done, buffers are swapped, showing only the complete scene.
        glfw.swap_buffers(window)

    
    glfw.terminate()
