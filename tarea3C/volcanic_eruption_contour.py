import numpy as np
import matplotlib.pyplot as mpl

H = 10000
W = 20000

def graficoContour(nombre):
    
    #Visualization time

    #Load the matrix
    ub = np.load(nombre)

    #This is to make sure the index is set properly
    #print(len(ub), len(ub[0]))
    #print(len(np.arange(0, W, W/len(ub))), len(np.arange(0, H, H/len(ub[0]))))


    X = np.arange(0, W, W/len(ub))
    Y = np.arange(0, H, H/len(ub[0]))
    X, Y = np.meshgrid(X, Y)
    Z = ub.T


    fig, ax = mpl.subplots()
    CS = ax.contour(X, Y, Z)
    ax.clabel(CS, inline=1, fontsize=5)


    mpl.xlabel('Eje horizontal [m]')
    mpl.ylabel('Eje vertical [m]')
    mpl.title('Curvas de nivel de potencial eléctrico [V] generado por volcán')


    mpl.show()