from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as sp2
import matplotlib.pyplot as mpl
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import sys 
import volcanic_eruption_color as v1
import volcanic_eruption_contour as v2
import volcanic_eruption_surf as v3

# Problem setup
H = 10000
W = 20000
h = int(sys.argv[1]) #Ejemplo: 250
E = int(sys.argv[2]) #Ejemplo: 3300
nombreDelArchivo = sys.argv[3] #Ejemplo: solution.npy

volcanoLeft   = 7550
volcanoRight = 13950
craterLeft    = 9050
craterRight  = 10950
VolcanoHeight = 2500

# Boundary Dirichlet Conditions:
TOP     = 0
BOTTOM  = 0
LEFT    = 0
RIGHT   = 0
VOLCANO = 0

# Number of unknowns

nh = int(W / h) - 1
nv = int(H / h) - 1

#print(nh, nv)

N = nh * nv

# We define a function to convert the indices from i,j to k and viceversa
# i,j indexes the discrete domain in 2D.
# k parametrize those i,j, this way we can tidy the unknowns
# in a column vector and use the standard algebra

def getK(i,j):
    return j * nh + i

def getIJ(k):
    i = k % nh
    j = k // nh
    return (i, j)
    
"""
# This code is useful to debug the indexation functions above
print("="*10)
print(getK(0,0), getIJ(0))
print(getK(1,0), getIJ(1))
print(getK(0,1), getIJ(2))
print(getK(1,1), getIJ(3))
print("="*10)

import sys
sys.exit(0)
"""

# In this matrix we will write all the coefficients of the unknowns
A = sp.csc_matrix((N, N))

# In this vector we will write all the right side of the equations
b = np.zeros((N,))

# Note: To write an equation is equivalent to write a row in the matrix system

# We iterate over each point inside the domain
# Each point has an equation associated
# The equation is different depending on the point location inside the domain


for j in range(0, nv):
    
    for i in range(0, nh):

        # We will write the equation associated with row k
        k = getK(i,j)

        # We obtain indices of the other coefficients
        k_up = getK(i, j+1)
        k_down = getK(i, j-1)
        k_left = getK(i-1, j)
        k_right = getK(i+1, j)

        # Depending on the location of the point, the equation is different
        
        #Crater
        if int(craterLeft /h) <= i and i <= int(craterRight /h) and j== int(VolcanoHeight /h):
            A[k, k_up] = 2
            A[k, k_right] = 1
            A[k, k_left] = 1
            A[k, k] = -4
            b[k] = 2 * h * E
        
        elif int(craterLeft /h) <= i and i <= int(craterRight /h) and 1<= j and j<= int(VolcanoHeight /h) - 1:
            A[k,k] = 1
            b[k]   = 0
            
        elif (int(volcanoLeft /h) + int((int(craterLeft /h)- int(volcanoLeft /h))*3/4)) <= i and i <= (int(craterRight /h) + int((int(volcanoRight /h) -(int(craterRight /h)))/3)) and 1<= j and j<= int((int(VolcanoHeight /h) - 1)*4/5):
            A[k,k] = 1
            b[k]   = 0
        
        elif (int(volcanoLeft /h) + int((int(craterLeft /h)- int(volcanoLeft /h))/2)) <= i and i <= (int(craterRight /h) + int((int(volcanoRight /h) -(int(craterRight /h)))*2/3)) and 1<= j and j<= int((int(VolcanoHeight /h) - 1)*3/5):
            A[k,k] = 1
            b[k]   = 0
        
        elif (int(volcanoLeft /h) + int((int(craterLeft /h)- int(volcanoLeft /h))/4)) <= i and i <= (int(craterRight /h) + int((int(volcanoRight /h) -(int(craterRight /h)))*2/3)) and 1<= j and j<= int((int(VolcanoHeight /h) - 1)*2/5):
            A[k,k] = 1
            b[k]   = 0
        
        elif (int(volcanoLeft /h)) <= i and i <= (int(volcanoRight /h)) and j <= 1 and j<=int((int(VolcanoHeight /h) -1)/5):
            A[k,k] = 1
            b[k]   = 0        
        
        # top side
        elif 1 <= i and i <= nh - 2 and j == nv - 1:
            A[k, k_down] = 1
            A[k, k_left] = 1
            A[k, k_right] = 1
            A[k, k] = -4
            b[k] = -TOP
        
        # left side
        elif i == 0 and 1 <= j and j <= nv - 2:
            A[k, k_up] = 1
            A[k, k_down] = 1
            A[k, k_right] = 1
            A[k, k] = -4
            b[k] = -LEFT
        
        # right side
        elif i == nh - 1 and 1 <= j and j <= nv - 2:
            A[k, k_up] = 1
            A[k, k_down] = 1
            A[k, k_left] = 1
            A[k, k] = -4
            b[k] = -RIGHT
        
        # bottom side
        elif 1 <= i and i <= nh - 2 and j == 0:
            A[k, k_up] = 1
            A[k, k_left] = 1
            A[k, k_right] = 1
            A[k, k] = -4
            b[k] = -BOTTOM
        
        # Interior 
        elif 1 <= i and i <= nh - 2 and 1 <= j and j <= nv - 2:
            A[k, k_up] = 1
            A[k, k_down] = 1
            A[k, k_left] = 1
            A[k, k_right] = 1
            A[k, k] = -4
            b[k] = 0
        
        # corner lower left
        elif (i, j) == (0, 0):
            A[k, k_up] = 1
            A[k, k_right] = 1
            A[k, k] = -4
            b[k] = -BOTTOM - LEFT

        # corner lower right
        elif (i, j) == (nh - 1, 0):
            A[k, k_up] = 1
            A[k, k_left] = 1
            A[k, k] = -4
            b[k] = -BOTTOM - RIGHT

        # corner upper left
        elif (i, j) == (0, nv - 1):
            A[k, k_down] = 1
            A[k, k_right] = 1
            A[k, k] = -4
            b[k] = -TOP - LEFT

        # corner upper right
        elif (i, j) == (nh - 1, nv - 1):
            A[k, k_down] = 1
            A[k, k_left] = 1
            A[k, k] = -4
            b[k] = -TOP - RIGHT


        else:
            print("Point (" + str(i) + ", " + str(j) + ") missed!")
            print("Associated point index is " + str(k))
            raise Exception()

# A quick view of a sparse matrix
#mpl.spy(A)

# Solving our system
x = sp2.spsolve(A, b)

# Now we return our solution to the 2d discrete domain
# In this matrix we will store the solution in the 2d domain
u = np.zeros((nh,nv))

for k in range(0, N):
    i,j = getIJ(k)
    u[i,j] = x[k]

# Adding the borders, as they have known values
ub = np.zeros((nh + 2, nv + 2))
ub[1:nh + 1, 1:nv + 1] = u[:,:]

# Dirichlet boundary condition
# top 
ub[0:nh + 2, nv + 1] = TOP
# bottom 
ub[0:nh + 2, 0] = BOTTOM
# left
ub[0, 1:nv + 1] = LEFT
# right
ub[nh + 1, 1:nv + 1] = RIGHT


np.save(nombreDelArchivo,ub)

#This will help us to find V above the crater
#x=int(len(ub)/2)
#y=int(len(ub[0])*3/4)
#print(ub[x][y])

v1.graficoColor(nombreDelArchivo)
v2.graficoContour(nombreDelArchivo)
v3.graficoSurf(nombreDelArchivo)