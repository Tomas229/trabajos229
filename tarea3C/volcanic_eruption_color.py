import numpy as np
import matplotlib.pyplot as mpl

H = 10000
W = 20000
def graficoColor(nombre):
    #Visualization time

    #Load the matrix
    ub = np.load(nombre)

    #This is to make sure the index is set properly
    #print(len(ub), len(ub[0]))
    #print(len(np.arange(0, W, W/len(ub))), len(np.arange(0, H, H/len(ub[0]))))


    X = np.arange(0, W, W/len(ub))
    Y = np.arange(0, H, H/len(ub[0]))
    X, Y = np.meshgrid(X, Y)
    Z = ub.T

    fig, ax = mpl.subplots(1,1)
    pcm = ax.pcolormesh(X,Y,Z, cmap='OrRd')
    fig.colorbar(pcm)
    ax.set_xlabel('Eje horizontal [m]')
    ax.set_ylabel('Eje vertical [m]')
    ax.set_title('Potencial elétrico [V] generado por un volcán en el plano xy')
    ax.set_aspect('equal', 'datalim')

    mpl.show()    
    


