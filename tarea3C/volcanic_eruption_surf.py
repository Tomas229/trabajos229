from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as mpl
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

H = 10000
W = 20000

def graficoSurf(nombre):
    #Visualization time

    #Load the matrix
    ub = np.load(nombre)

    fig = mpl.figure()
    ax = fig.gca(projection='3d')

    #This is to make sure the index is set properly
    #print(len(ub), len(ub[0]))
    #print(len(np.arange(0, W, W/len(ub))), len(np.arange(0, H, H/len(ub[0]))))


    X = np.arange(0, W, W/len(ub))
    Y = np.arange(0, H, H/len(ub[0]))
    X, Y = np.meshgrid(X, Y)
    Z = ub.T


    surf = ax.plot_surface(X, Y, Z, cmap='OrRd',linewidth=0, antialiased=False)

    ax.set_xlabel('Eje horizontal [m]')
    ax.set_ylabel('Eje vertical [m]')
    ax.set_zlabel('Potencial eléctrico [V]')
    ax.set_title('Potencial elétrico generado por un volcán en el plano xy')

    mpl.show()